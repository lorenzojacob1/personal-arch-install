#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Brussels /etc/localtime
hwclock --systohc
reflector -c Belgium -a 6 --sort rate --save /etc/pacman.d/mirrorlist
sed -i '171s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=US" >> /etc/vconsole.conf
echo "icebreaker" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 icebreaker.localdomain icebreaker" >> /etc/hosts

echo "Enter root password"
passwd

pacman -S -y grub efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools base-devel linux-headers xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion openssh rsync reflector acpi acpi_call virt-manager qemu edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld flatpak sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font # tlp for laptop power management

pacman -S --noconfirm xf86-video-amdgpu
# pacman -S --noconfirm nvidia nvidia-utils nvidia-settings

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB

grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
# systemctl enable tlp
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m lorenzo
echo "Enter user password"
passwd lorenzo
usermod -aG libvirt lorenzo

echo "lorenzo ALL=(ALL) ALL" >> /etc/sudoers.d/lorenzo

echo "DONE!"

pacman -S --noconfirm xorg sddm plasma kde-applications plasma-wayland-session vlc

sed -i '90s/.//' /etc/pacman.conf
sed -i '91s/.//' /etc/pacman.conf

pacman -Syyu

pacman -S --noconfirm steam

systemctl enable sddm
/bin/echo -e "DONE! exit, umount -a, reboot"
